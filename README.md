# README #

### What is this repository for? ###

This is just a rewrite of an earlier project that was written in Django: https://bitbucket.org/andresespinosapc/micontacto

This is one of my earliest projects that I just wanted to make public instead of deleting it.
It served mostly to learn, because the project never went forward.

The idea was an entrepreneurship social network where you could post your products and your company details, ambitions, etc.
It was mainly focused on small and local entrepreneurship, to help people promote their products and find other entrepreneurs to help each other.

### Run

- Install Meteor: `curl https://install.meteor.com/ | sh`
- Install required packages: `npm install`
- Run: `npm start`