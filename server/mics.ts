import {Meteor} from 'meteor/meteor';
import {Mics} from '../collections/mics.ts';
//noinspection TypeScriptCheckImport
import {ReactiveAggregate} from 'meteor/jcbernack:reactive-aggregate';

Meteor.publish('mics', function() {
   return Mics.find({}, {
       name: 1,
       logo: 1,
       description: 1
   });
});

Meteor.publish('userMics', function(userId: string) {
    // isOwner
    if (this.userId == userId) {
        return Mics.find({
            'owners.id': this.userId
        }, {
            fields: {
                name: 1,
                logo: 1,
                description: 1
            }
        });
    }
    else {
        console.log('not mine');

        ReactiveAggregate(this, Mics, [{
            $match: {
                $or: [
                    {admins: userId},
                    {owners: userId}
                ]
            }
        },
        {
            $project: {
                admins: {$cond: ["$anonymous", null, "$admins"]},
                owners: {$cond: ["$anonymous", null, "$owners"]},
                name: 1,
                logo: 1,
                description: 1
            }
        }]);
    }
});