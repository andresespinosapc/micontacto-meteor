import { Accounts } from 'meteor/accounts-base';
import {Meteor} from 'meteor/meteor';

Accounts.onCreateUser((options, user) => {
    user.firstName = options.firstName;
    user.lastName = options.lastName;

    return user;
});

Meteor.publish('users', () => {
    return Meteor.users.find({}, {
        fields: {
            _id: 1,
            firstName: 1,
            lastName: 1
        }
    });
});