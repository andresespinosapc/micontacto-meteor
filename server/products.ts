import {Meteor} from 'meteor/meteor';
import {Products} from '../collections/products.ts';

Meteor.publish('products', function() {
    return Products.find({}, {
        micId: 1,
        name: 1,
        description: 1,
        price: 1,
        mainPhoto: 1
    });
});