interface Tag {
    _id ? : string;
    tag: string;
    products: Array<Product>;
    mics: Array<Mic>;
}