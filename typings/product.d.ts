interface Product {
    _id ? : string;
    micId: string;
    name: string;
    description: string;
    price ? : number;
    mainPhoto ? : string;
    tags: Array<Tag>;
}