interface Permission {
    mic: Array<string>;
    story: Array<string>;
    products: Array<string>;
    owners: Array<string>;
}

interface Owner {
    id: string;
    permissions: Permission;
}

interface Mic {
    _id ? : string;
    owners: Array<Owner>;
    name: string;
    logo ? : any;
    description: string;
    anonymous: boolean;
    tags: Array<Tag>;
}