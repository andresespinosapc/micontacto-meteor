import {Mongo} from 'meteor/mongo';
//noinspection TypeScriptCheckImport
import SimpleSchema from 'simpl-schema';
import {Mics} from './mics.ts';

export let Products = new Mongo.Collection<Product>('products');

let isAllowed = function(action, userId, doc) {
    // Check that Mic exists
    let mic = Mics.findOne(doc.micId);
    for (let i=0; i<mic.owners.length; i++) {
        let owner = mic.owners[i];
        if (owner.id == userId) {
            if (owner.permissions.products.indexOf(action) != -1)
                return true;
        }
    }

    return false;
};

Products.allow({
    insert: function(userId, doc) {
        return isAllowed('add', userId, doc);
    },
    update: function(userId, doc) {
        return isAllowed('modify', userId, doc);
    },
    remove: function(userId, doc) {
        return isAllowed('remove', userId, doc);
    }
});

let ProductSchema = new SimpleSchema({
    micId: {
        type: String,
        // regEx: SimpleSchema.RegEx.Id
    },
    name: {
        type: String
    },
    description: {
        type: String
    },
    price: {
        type: Number,
        optional: true
    },
    mainPhoto: {
        type: String,
        optional: true,
        regEx: SimpleSchema.RegEx.Url
    },
    tags: {
        type: Array
    },
    'tags.$': {
        type: String,
        regEx: /^[a-z0-9\-]+$/
    }
});

(<any>Products).attachSchema(ProductSchema);