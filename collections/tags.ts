import {Mongo} from 'meteor/mongo';
//noinspection TypeScriptCheckImport
import SimpleSchema from 'simpl-schema';

export let Tags = new Mongo.Collection<Tag>('tags');

// Tags.allow({
//     insert: () => {return false;},
//     update: () => {return false;},
//     remove: () => {return false;}
// });

let TagSchema = new SimpleSchema({
    tag: {
        type: String,
        unique: true,
        regEx: /^[a-z0-9\-]+$/,
        autoValue: function() {
            if (this.isSet) {
                return this.value.toLowerCase();
            }
        }
    },
    mics: {
        type: Array
    },
    'mics.$': {
        type: String
        // regEx: SimpleSchema.RegEx.Id
    },
    products: {
        type: Array
    },
    'products.$': {
        type: String
        // regEx: SimpleSchema.RegEx.Id
    }
});

(<any>Tags).attachSchema(TagSchema);