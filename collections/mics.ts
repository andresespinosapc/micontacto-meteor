import {Mongo} from 'meteor/mongo';
//noinspection TypeScriptCheckImport
import SimpleSchema from 'simpl-schema';


export let Mics = new Mongo.Collection<Mic>('mics');

let isAllowed = function(action, userId, doc) {
    for (let i=0; i<doc.owners.length; i++) {
        let owner = doc.owners[i];
        if (owner.id == userId) {
            if (owner.permissions.mic.indexOf(action) != -1)
                return true;
        }
    }

    return false;
};

Mics.allow({
    insert: function() {
        // if (doc.owners[0]._id == userId) {
        //     return true;
        // }
        return true;
    },
    update: function(userId, doc) {
        return isAllowed('modify', userId, doc);
    },
    remove: function(userId, doc) {
        return isAllowed('remove', userId, doc);
    }
});

// (<any>Mics).before.insert(function(userId, doc) {
//     console.log('before insert');
//     doc.owners = [Meteor.users.findOne(userId)];
// });

let PermissionSchema = new SimpleSchema({
    mic: {
        type: Array
    },
    'mic.$': {
        type: String,
        allowedValues: ['modify', 'remove']
    },
    story: {
        type: Array
    },
    'story.$': {
        type: String,
        allowedValues: ['add', 'modify', 'remove']
    },
    products: {
        type: Array
    },
    'products.$': {
        type: String,
        allowedValues: ['add', 'modify', 'remove']
    },
    owners: {
        type: Array
    },
    'owners.$': {
        type: String,
        allowedValues: ['add', 'remove']
    }
});

let OwnerSchema = new SimpleSchema({
    id: {
        type: String,
        // regEx: SimpleSchema.RegEx.Id
    },
    permissions: {
        type: PermissionSchema
    }
});

let MicSchema = new SimpleSchema({
    // _id: {
    //     type: String,
    //     // regEx: SimpleSchema.RegEx.Id
    // },
    owners: {
        type: Array,
        autoValue: function() {
            if (this.userId) {
                return [{
                    id: this.userId,
                    permissions: {
                        mic: ['modify', 'remove'],
                        story: ['add', 'modify', 'remove'],
                        products: ['add', 'modify', 'remove'],
                        owners: ['add', 'remove']
                    }
                }];
            }
            else {
                this.unset();
            }
        }
    },
    'owners.$': {
        type: OwnerSchema
    },
    name: {
        type: String,
        unique: true
    },
    logo: {
        type: String,
        optional: true,
        regEx: SimpleSchema.RegEx.Url
    },
    description: {
        type: String
    },
    anonymous: {
        type: Boolean
    },
    tags: {
        type: Array
    },
    'tags.$': {
        type: String,
        regEx: /^[a-z0-9\-]+$/
    }
});

(<any>Mics).attachSchema(MicSchema);