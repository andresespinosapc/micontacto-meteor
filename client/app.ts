import 'reflect-metadata';
import { Component, provide } from '@angular/core';
import { provideRouter, RouterConfig, Router, ROUTER_DIRECTIVES } from '@angular/router';
import { bootstrap } from 'angular2-meteor-auto-bootstrap';
import { APP_BASE_HREF } from '@angular/common';
import { MeteorComponent } from 'angular2-meteor';
import { Meteor } from 'meteor/meteor';
import { FormBuilder, ControlGroup, Validators } from '@angular/forms';
import { Accounts } from 'meteor/accounts-base'
import { InjectUser } from 'angular2-meteor-accounts-ui';
import {MicsView} from './imports/micsView/micsView';
import {EntrepreneursView} from './imports/entrepreneursView/entrepreneursView';
import {StoriesView} from './imports/storiesView/storiesView';
import {ProductsView} from './imports/productsView/productsView';

// import profileScript from './imports/profile/main.js';

//noinspection TypeScriptCheckImport
import template from './app.html';
import {Profile} from "./imports/profile/profile";
import {MicProfile, MicProfileStory, MicProfileProducts, MicProfileGallery} from "./imports/micProfile/micProfile";


@Component({
    selector: 'app',
    template,
    directives: [ROUTER_DIRECTIVES]
})
@InjectUser('user')
class MiContacto extends MeteorComponent {
    user: Meteor.User;
    signupForm: ControlGroup;
    signupError: string;
    loginForm: ControlGroup;
    loginError: string;


    constructor(private router: Router) {
        super();

        // router.events.subscribe((event: Event) => {
        //     console.log('router event!');
        //     if(event instanceof NavigationEnd) {
        //         console.log('navigation end');
        //         console.log(this);
        //         console.log(event);
        //
        //         // profileScript();
        //     }
        //     // NavigationEnd
        //     // NavigationCancel
        //     // NavigationError
        //     // RoutesRecognized
        // });

        let signupFb = new FormBuilder();

        this.signupForm = signupFb.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.signupError = '';

        let loginFb = new FormBuilder();

        this.loginForm = loginFb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.loginError = '';
    }

    signup(credentials) {
        if (this.signupForm.valid) {
            console.log('valid');
            Accounts.createUser({
                email: credentials.email,
                password: credentials.password,
                firstName: credentials.firstName,
                lastName: credentials.lastName
            }, (err) => {
                if (err) {
                    this.signupError = err;
                }
                else {
                    this.router.navigate(['/']);
                }
            });
        }
        else {
            console.log('non valid');
        }
    }

    login(credentials) {
        console.log('login');
        if (this.loginForm.valid) {
            console.log('valid');
            Meteor.loginWithPassword(credentials.email, credentials.password, (err) => {
                if (err) {
                    console.log(err);
                    this.loginError = err;
                }
                else {
                    this.router.navigate(['/']);
                }
            });
        }
    }

    logout() {
        this.autorun(() => {
            Meteor.logout();
        });
    }
}

const routes: RouterConfig = [
    { path: '',                     redirectTo: 'mics', pathMatch: 'full' },
    { path: 'mics',              	component: MicsView },
    { path: 'stories',	            component: StoriesView },
    { path: 'products',	            component: ProductsView },
    { path: 'entrepreneurs',	    component: EntrepreneursView },
    { path: 'profile/:userId',	    component: Profile },
    { path: 'micProfile/:micId',	component: MicProfile, children: [
        { path: '',                     redirectTo: 'story', pathMatch: 'full' },
        { path: 'story',                component: MicProfileStory },
        { path: 'products',             component: MicProfileProducts },
        { path: 'gallery',              component: MicProfileGallery }
    ] }
    // { path: '**',               component: PageNotFoundComponent }
];

const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];

bootstrap(MiContacto, [
  APP_ROUTER_PROVIDERS, 
  // provide(APP_BASE_HREF, { useValue: '/' })
  {
    provide: APP_BASE_HREF,
    useValue: '/',
  },
]);