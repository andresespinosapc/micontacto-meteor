import 'reflect-metadata';
import { Component } from '@angular/core';

//noinspection TypeScriptCheckImport
import template from './storiesView.html';

@Component({
    selector: 'storiesView',
    template
})
export class StoriesView {
    upload() {
        let files = $("input.file_bag")[0].files;
        console.log(files);

        S3.upload({
            files: files,
            path: "subfolder"
        }, function (e, response){
            console.log(response);
        });
    }
}