import 'reflect-metadata';
import { Component } from '@angular/core';
import {Mics} from "../../../collections/mics";
import { ActivatedRoute, Router, ROUTER_DIRECTIVES } from '@angular/router';
import { FormBuilder, ControlGroup, Validators } from '@angular/common';

//noinspection TypeScriptCheckImport
import template from './micProfile.html';
//noinspection TypeScriptCheckImport
import templateStory from './micProfileStory.html';
//noinspection TypeScriptCheckImport
import templateProducts from './micProfileProducts.html';
//noinspection TypeScriptCheckImport
import templateGallery from './micProfileGallery.html';
import {MeteorComponent} from "angular2-meteor";
import {InjectUser} from "angular2-meteor-accounts-ui";

import {hideModal} from '../js/main.js';
import micProfileScript from './main.js';
import {Products} from "../../../collections/products";

@Component({
    selector: 'micProfile',
    template,
    directives: [ROUTER_DIRECTIVES]
})
@InjectUser()
export class MicProfile extends MeteorComponent {
    user: Meteor.User;
    mic: Mic;
    myMic: boolean = false;
    canFollow: boolean = false;

    constructor(private route: ActivatedRoute) {
        super();
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.subscribe('mics', () => {
                this.mic = Mics.findOne(params['micId']);

                if (this.user) {
                    if (this.mic.owners.map(function(e) { return e.id; }).indexOf(this.user._id) != -1) {
                        this.myMic = true;
                    }
                    else {
                        this.canFollow = true;
                    }
                }
            }, true);
        });
    }
}

@Component({
    selector: 'micProfileStory',
    template: templateStory
})
export class MicProfileStory { }

@Component({
    selector: 'micProfileProducts',
    template: templateProducts
})
@InjectUser()
export class MicProfileProducts extends MeteorComponent {
    products: Mongo.Cursor<Product>;
    canAdd: boolean = false;
    mic: Mic;
    micId: string;
    user: Meteor.User;
    createProductForm: ControlGroup;
    createProductError: string;

    constructor(private router: Router) {
        super();

        let createProductFb = new FormBuilder();

        this.createProductForm = createProductFb.group({
            name: ['', Validators.required],
            description: [''],
            price: ['']
        });
    }

    createProduct(credentials) {
        if (this.createProductForm.valid) {
            console.log('valid');

            Products.insert(<Product>{
                micId: this.micId,
                name: credentials.name,
                description: credentials.description,
                price: credentials.price
            }, (err) => {
                if (err) {
                    console.log(err);
                    this.createProductError = err;
                }
                else {
                    hideModal();
                }
            });
        }
        else {
            console.log('not valid');
            console.log(this.createProductForm.value);
        }
    }

    ngOnInit() {
        //noinspection TypeScriptUnresolvedVariable
        this.micId = this.router.routerState._root.children[0].value.params._value.micId;

        this.subscribe('mics', () => {
            this.mic = Mics.findOne(this.micId);

            if (this.user) {
                for (let i=0; i<this.mic.owners.length; i++) {
                    let owner = this.mic.owners[i];
                    if (owner.id == this.user._id) {
                        if (owner.permissions.products.indexOf('add') != -1)
                            this.canAdd = true;
                    }
                }
            }

            micProfileScript();
        }, true);

        this.subscribe('products', () => {
            this.products = Products.find({
                micId: this.micId
            });
        });
    }
}

@Component({
    selector: 'micProfileGallery',
    template: templateGallery
})
export class MicProfileGallery { }