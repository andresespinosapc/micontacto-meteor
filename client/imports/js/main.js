$(document).ready(function() {
    // $('#main-searchbar').attr('width', '90%');
    $('.ui.dropdown').dropdown();

    $('#signup-modal').modal({
        observeChanges: true
    });
    $('#login-modal').modal({
        observeChanges: true
    });

    $('#signup-button').click(function () {
        $('#signup-modal').modal('show');
    });

    $('#login-button').click(function () {
        $('#login-modal').modal('show');
    });
});

export function hideModal() {
    console.log('hiding modal');
    $('.modal').modal('hide');
}