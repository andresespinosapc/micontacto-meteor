import 'reflect-metadata';
import { Component } from '@angular/core';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {MeteorComponent} from "angular2-meteor";

//noinspection TypeScriptCheckImport
import template from './entrepreneursView.html';

@Component({
    selector: 'entrepreneursView',
    template,
    directives: [ROUTER_DIRECTIVES]
})
export class EntrepreneursView extends MeteorComponent {
    entrepreneurs: Mongo.Cursor<Meteor.User>;

    ngOnInit() {
        this.subscribe('users', () => {
            this.entrepreneurs = Meteor.users.find();
        }, true);
    }
}