import 'reflect-metadata';
import { Component } from '@angular/core';

//noinspection TypeScriptCheckImport
import template from './micsView.html';
import {MeteorComponent} from "angular2-meteor";
import {Mics} from "../../../collections/mics";
import {ROUTER_DIRECTIVES} from "@angular/router";

@Component({
    selector: 'micsView',
    template,
    directives: [ROUTER_DIRECTIVES]
})
export class MicsView extends MeteorComponent {
    mics: Mongo.Cursor<Mic>;

    ngOnInit() {
        this.subscribe('mics', () => {
            this.mics = Mics.find();
        }, true);
    }
}