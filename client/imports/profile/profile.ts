import 'reflect-metadata';
import { Component } from '@angular/core';
import { InjectUser } from 'angular2-meteor-accounts-ui';
import {MeteorComponent} from 'angular2-meteor';
import { Meteor } from 'meteor/meteor';
import { ActivatedRoute, ROUTER_DIRECTIVES } from '@angular/router';
import { FormBuilder, ControlGroup, Validators } from '@angular/common';
import {Mongo} from 'meteor/mongo';
// import {Dropzone} from '../js/dropzone.js';

import {hideModal} from '../js/main.js';
import profileScript from './main.js';


//noinspection TypeScriptCheckImport
import template from './profile.html';
import {Mics} from "../../../collections/mics";

@Component({
    selector: 'profile',
    template,
    directives: [ROUTER_DIRECTIVES]
})
@InjectUser('user')
export class Profile extends MeteorComponent {
    user: Meteor.User;
    profileUser: Meteor.User;
    profileUserId: string;
    myProfile: boolean = false;
    canFollow: boolean = false;
    createMicForm: ControlGroup;
    createMicError: string;
    anonymousCb: boolean = false;
    userMics: Mongo.Cursor<Mic>;

    constructor(private route: ActivatedRoute) {
        super();

        let createMicFb = new FormBuilder();

        this.createMicForm = createMicFb.group({
            name: ['', Validators.required],
            description: ['']
        });
    }

    createMic(credentials) {
        if (this.createMicForm.valid) {
            console.log('valid');
            //noinspection TypeScriptUnresolvedFunction,TypeScriptValidateTypes
            Mics.insert({
                // name: this.createMicForm.value.name,
                // description: this.createMicForm.value.description,
                name: credentials.name,
                description: credentials.description,
                anonymous: this.anonymousCb
            }, (err) => {
                if (err) {
                    console.log(err);
                    this.createMicError = err;
                }
                else {
                    hideModal();
                }
            });
        }
        else {
            console.log('not valid');
            console.log(this.createMicForm.value);
        }
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.profileUserId = params['userId'];

            this.subscribe('users', this.profileUserId, () => {
                this.profileUser = Meteor.users.findOne(this.profileUserId);

                if (this.user) {
                    if (this.profileUser._id == this.user._id) {
                        this.myProfile = true;
                    }
                    else {
                        this.canFollow = true;
                    }
                }

                profileScript();
            }, true);

            this.subscribe('userMics', this.profileUserId, () => {
                this.userMics = Mics.find();
            }, true);

            // console.log(Dropzone);
            // let myDropzone = new Dropzone('div#uploadBox');
        });
    }
}