export default function() {
    console.log('profile executed');

    $('#createmic-modal').modal({
        observeChanges: true
    });

    $('#createmic-button').click(function() {
        console.log('clicked');
        $('#createmic-modal').modal('show');
    });

    $('.ui.checkbox').checkbox();
}
