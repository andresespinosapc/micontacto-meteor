import 'reflect-metadata';
import { Component } from '@angular/core';
import {MeteorComponent} from "angular2-meteor";
import {Products} from "../../../collections/products";
import {ROUTER_DIRECTIVES} from "@angular/router";

//noinspection TypeScriptCheckImport
import template from './productsView.html';

@Component({
    selector: 'productsView',
    template,
    directives: [ROUTER_DIRECTIVES]
})
export class ProductsView extends MeteorComponent {
    products: Mongo.Cursor<Product>;

    ngOnInit() {
        this.subscribe('products', () => {
            this.products = Products.find();
        }, true);
    }
}